///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// Header file for Node class
///
/// @file node.hpp
/// @version 1.0
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   1 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

namespace animalfarm {

class Node{
   friend class SingleLinkedList;
protected:
   Node* next = nullptr;

   friend class SingleLinkList;
};

}

