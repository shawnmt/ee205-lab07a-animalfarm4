///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   1 Mar 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string newTagID, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Branta sandvicensis";
   featherColor = newColor;
   isMigratory = true;
   tagID = newTagID;
}

const string Nene::speak() {
   return string ( "Nay, nay" );
}

/// Print out Nene and the information Bird holds.
void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tagID << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm


