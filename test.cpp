///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   1 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <random>

#include "animal.hpp"
#include "animalF.hpp"
#include "node.hpp"
#include "list.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   cout << "Hello World!" << endl;
   
   Node node;

   SingleLinkedList list;
   
   return 0;
}

