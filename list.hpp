///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// Header file for List class
///
/// @file list.hpp
/// @version 1.0
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   1 Apr 2021
////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

namespace animalfarm{

class SingleLinkedList {
protected:
   Node* head = nullptr;
   unsigned int count = 0; 
public:
   const bool  empty() const;
   void        push_front( Node* newNode );
   Node*       pop_front() ;
   Node*       get_first() const;
   Node*       get_next( const Node* currentNode ) const;
   unsigned int      size() const;
};

}


