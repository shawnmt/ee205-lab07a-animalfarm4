///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <random>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
	
   Animal::Animal(void) {cout << '.';};
   Animal::~Animal(void) {cout << 'x';};

void Animal::printInfo() {
	
   cout << "   Species = [" << species << "]" << endl;
   cout << "   Gender = [" << genderName( gender ) << "]" << endl;

}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK:  return string("Black"); break;
      case WHITE:  return string("White"); break;
      case RED:    return string("Red"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN:  return string("Brown"); break;
   }
   return string("Unknown");
};

const Gender Animal::getRandomGender() {
   random_device rd;
   int random = rd() % 2; //two genders to pick form
   return Gender(random);
};

const Color Animal::getRandomColor(){
   random_device rd;
   int random = rd() % 6; //6 colors to choose from
   return Color (random);
};

const bool Animal::getRandomBool(){
   random_device rd;
   int random = rd() % 2;
   switch (random){
      case(0): false; break;
      case(1): true; break;
   }
   return 0;
};

const float Animal:: getRandomWeight( const float from, const float to){
   int  range = (int) (to - from);
   float randomWeight = from + (rand()%range);
      
   return randomWeight;

};

const string Animal::getRandomName(){
   random_device rd;
   int random = (rd() % 6 ) + 4;
   string randomName;
   randomName += (char)(rd() % 26 ) + 65;
   for(int i = 1; i < random; i++){
   
      randomName += (char)(rd() % 26 ) + 97;
   
   }
   return randomName;
};

} // namespace animalfar


